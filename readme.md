# Projeto
"
- API com framework fastAPI e Servidor Uvicorn
- Banco de dados postgres 
"

# Subindo a App

    uvicorn main:app --reload
ou
    execute main.py 
    
    if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="127.0.0.1", port=8000)

## Documentação padrão da API
    http://127.0.0.1:8000/docs

## Documentação alternativa da API

    http://127.0.0.1:8000/redoc


# DEVELOPMENT

- Criar venv e instalar requeriments.txt

ou

    pip install uvicorn fastapi
    pip install sqlalchemy
    pip install psycopg2-binary

## criar dir app/
    mkdir app
    cd app


## criar main.py inicial dentro da app


    from fastapi import FastAPI
    from model import *

    app = FastAPI()

    @app.get('/')
    async def Home():
        return "Home fastApi

## Na pasta app, rode uvicorn
    uvicorn main:app --reload
    
    localhost:8000

## Crie config.py com url conexão e sqlalchemy

    from sqlalchemy import create_engine
    from sqlalchemy.orm import sessionmaker
    from sqlalchemy.ext.declarative import declarative_base

    DATABASE_URL = "postgresql://user:pass@localhost:5432/nome_db"

    engine = create_engine(DATABASE_URL)
    SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    Base = declarative_base()


## Crie os modelos

    models.py

## Add imports em main.py

    import models

    from config import engine

## Alterar main.py

    model.Base.metadata.create_all(bind=engine)


## Crie o schemas.py
    schemas postgres

## Crie crud.py
    def get()
    def get_id()
    def create()
    def remove()
    def update()

## Crie as Rotas, routes.py

    @router.get("/")
    async def get_books(db: Session = Depends(get_db)):
        _books = crud.get_book(db)
        return Response(status="Ok", code="200", message="Success fetch data", result=_books)


## Altere main novamente
    app.include_router(router, prefix="/book", tags=["book"])

# ENDPOINTS
    http://127.0.0.1:8000/docs

## POST para criar

-   http://127.0.0.1:8000/book/create

    {
    "parameter": {
        "title": "python",
        "description": "python book"
    }
    }

## GET para buscar

- Top 100:
    http://127.0.0.1:8000/book/

- Id:
    http://127.0.0.1:8000/book/id

## PATCH para alterar (update)

-    http://127.0.0.1:8000/book/update

    {
    "parameter": {
        "id": 3,
        "title": "python update",
        "description": "python book update"
    }
    }

## DELETE para excluir dados

-    http://127.0.0.1:8000/book/delete

    {
    "parameter": {
        "id": 3
    }
    }


# Dicas GIT


## git init
-   cria repositório git local criado e com branch master em vez de main (normalmente)

## adicione o repositório local ao remoto passando endereço do projeto criado no gitlab
	git remote add origin <endereço-do-repositório-GitLab>

## add primeiro alterações
    git add .

## primeiro commit
    git commit -m "Initial commit"

## mude o nome da branch (caso repositório no gitlab)
	git branch -m main

## envie o primeiro push para main
	git push -u origin main


