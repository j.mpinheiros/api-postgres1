from fastapi import FastAPI
import model
from routes import router
from config import engine


# criar todos os modelos
model.Base.metadata.create_all(bind=engine)


# iniciar app
app = FastAPI()


#Rota Home inicial
@app.get('/')
async def Home():
    return "Home fastApi"


# Incluir todas as rotas em routes com padrão 8000/book/
app.include_router(router, prefix="/book", tags=["book"])


"""
Caso queira executar o arquivo main,
em vez de iniciar uvicorn no terminal
"""
if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="127.0.0.1", port=8000)