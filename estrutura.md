## Estrutura Projeto

<br />

The project is coded using a simple and intuitive structure presented below:

```bash
< PROJECT ROOT >
   |
   |-- app/                            
   |    |-- config.py                  # Project Configuration  
   |    |-- models.py                  # APP Models 
   |    |-- main.py                    # Project main 
   |    |-- schemas.py                 # Project schemas postgres
   |    |-- crud.py                    # functions CRUD
   |    |-- routes.py                  # Project Routing
   |     
   |-- requirements.txt                  # Project Dependencies
************************************************************************
```
<br />

# Fluxo lógico da app

    config -> models -> main -> schemas -> crud -> routes

